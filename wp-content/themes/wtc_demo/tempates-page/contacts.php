<?php  /* Template Name: Contacts */
get_header();
 ?>
<main class="main">
	<?php do_action('contact_form_tamplate_action'); ?>
				<section class="section section-zero">
					<div class="container">
						<div class="section-title">Ми на мапі</div>
						<a href="https://goo.gl/maps/PcFdkC9qVwhPN69UA" target="_blank" class="map_link">
                            <img src="<?php do_action('contact_form_tamplate_action');?>/img/contact-map.png" alt="map">
                        </a>
					</div>
				</section>
</main>
<?php
get_footer();
?>
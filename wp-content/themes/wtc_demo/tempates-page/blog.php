<?php  /* Template Name: Blog */
get_header();
 ?>
<main class="main">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/pagination.css">
				<section class="section section_title-slide">
                    <div class="title-slide">
                        <div class="title-slide_text">
                            <div class="container">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current">Блог</span>
                                    </li>
                                </ul>
                                <h1 class="section-title">Блог</h1>
                            </div>
                        </div>
                        <div class="title-slide_img">
                            <img src="<?php echo get_template_directory_uri()?>/img/title-slide_2.png" alt="">
                        </div>
                    </div>
				</section>
                <section class="section section--pt">
					<div class="container">
                        <div class="blog-line">
                            <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => 2, // Кількість записів на сторінці
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'paged' => $paged,
                        );

                        $query = new WP_Query($args);

                        if ($query->have_posts()) :

                            while ($query->have_posts()) : $query->the_post();

                                if (has_post_thumbnail()) {
                                    $image_url = get_the_post_thumbnail_url($post->ID, 'full');
                                }

                                echo '
                                <a href="' . get_permalink() . '" class="blog-home-item">
                                    <span class="blog-home-item__pct">
                                        <img src="' . esc_url($image_url) . '" alt="' . get_the_title() . '">
                                    </span>
                                    <span class="blog-home-item__title">' . get_the_title() . '</span>
                                    <span class="blog-home-item__desc">' . wp_trim_words(get_the_content(), 10) . '</span>
                                    <span class="blog-home-item__more">Детальніше</span>
                                </a>';

                            endwhile;


                        else :
                            echo 'Пости не знайдено.';
                        endif;
                        ?>            
                        </div>
                        <nav class="box-pagination">
                            <?php
                                echo '<div class="">';
                                wp_pagenavi(array('query' => $query));
                                echo '</div>';
                                                    
                                wp_reset_postdata();
                            ?>
                        </nav>
                    </div>
                </section>
				<?php do_action('contact_form_tamplate_action'); ?>
			</main>
<?php
get_footer();
?>
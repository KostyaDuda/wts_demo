<?php  /* Template Name: Service */
get_header();
 ?>
<main class="main">
				<section class="section section_title-slide">
                    <div class="title-slide title-slide_full">
                        <div class="title-slide_text">
                            <div class="container">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current">Сервіс та гарантії</span>
                                    </li>
                                </ul>
                                <h1 class="section-title">Сервісне обслуговування</h1>
                                <p>
                                    WinTechSolution надає своїм клієнтам якісне сервісне обслуговування та ремонт усіх вузлів та агрегатів.
                                </p>
                            </div>
                        </div>
                        <div class="title-slide_img">
                            <img src="<?php echo get_template_directory_uri()?>/img/title-full_1.png" alt="">
                        </div>
                    </div>
				</section>
                <section class="section section--pt">
					<div class="container">
                        <div class="section-title">Наші переваги:</div>
                        <div class="advent_wrap">
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_1.svg" alt="icon">
                                </div>
                                <p>
                                    Інженери проходять навчання від завода-виробника
                                </p>
                            </div>
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_2.svg" alt="icon">
                                </div>
                                <p>
                                    Сервісні автомобілі оснащені усім необхідним інструментом 
                                    та обладнанням для проведення ремонту та діагностики
                                </p>
                            </div>
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_3.svg" alt="icon">
                                </div>
                                <p>
                                    Компанія WinTechSolution має повну технічну підтримку заводу Reno
                                </p>
                            </div>
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_4.svg" alt="icon">
                                </div>
                                <p>
                                    Високий процент ремонтів виконаних з першого разу, 
                                    що мінімізує час простою техніки
                                </p>
                            </div>
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_5.svg" alt="icon">
                                </div>
                                <p>
                                    Техніка, що обслужена офіційним дилером, має 
                                    більш високу ціну на вторичному ринку
                                </p>
                            </div>
                            <div class="advent_item">
                                <div class="advent_item-img">
                                    <img src="<?php echo get_template_directory_uri()?>/img/advent-icon_6.svg" alt="icon">
                                </div>
                                <p>
                                    Конкурентоспромо<br>жні ціни на сервісні послуги
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section section--pt section-gradient section-gradient--right">
					<div class="container">
						<div class="section-title section-title_lim">В рамках сервісної програми ми пропонуємо:</div>
                        <ul class="services-list">
                            <li class="services-item">
                                Передпродажну підготовку техніки
                            </li>
                            <li class="services-item">
                                Навчання операторів основам правильної експлуатації
                            </li>
                            <li class="services-item">
                                Діагностику несправностей
                            </li>
                            <li class="services-item">
                                Поставку оригінальних запчастин та мастильних матеріалів
                            </li>
                            <li class="services-item">
                                Поставку оригінальних запчастин та мастильних матеріалів
                            </li>
                            <li class="services-item">
                                Підбір мастильних матеріалів для вашої техніки
                            </li>
                            <li class="services-item">
                                Гарантійне та постгарантійне обслуговування техніки
                            </li>
                        </ul>
                    </div>
                </section>
                <?php do_action('contact_form_tamplate_action'); ?>
</main>
<?php
get_footer();
?>
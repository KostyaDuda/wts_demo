<?php  /* Template Name: Categoty_Product */
get_header();

$data_category = get_category_by_id($_GET['id']);
$data_products = get_list_product_by_categeory($_GET['id']);


 ?>
 
<main class="main">
				<section class="section section-slide section-gradient section-gradient--right">
					<div class="container">
						<div class="category-slide">
							<div class="category-slide__img">
								<img src="<?php echo get_template_directory_uri()?>/img/category_slide.png" alt="">
							</div>
							<div class="category-slide__text">
								<ul class="breadcrumb">
									<li class="breadcrumb__item">
										<a href="/" class="breadcrumb__link">Головна</a>
									</li>
									<li class="breadcrumb__item">
										<span class="breadcrumb__current"><?php  echo $data_category[0]['name']; ?></span>
									</li>
								</ul>
								<h1 class="section-title"><?php  echo $data_category[0]['name']; ?></h1>
								<p>
								<?php  echo $data_category[0]['description']; ?>
								</p>
								<a href="/product" class="btn btn_medium">Переглянути модельний ряд</a>
							</div>
						</div>
					</div>
				</section>
				<section class="section section-white" id="model-list">
					<div class="container">
						<h3 class="section-title">Модельний ряд "<?php  echo $data_category[0]['name']; ?>"</h3>
						<div class="product-list">
							<?php 
							foreach($data_products as $product){
								echo '
								<a href="/product?id='.$product['id'].'" class="product-item">
								<span class="product-item__title">'.$product['name'].'</span>
								<span class="product-item__pct">
									<img src="'.$product['img1'].'" alt="">
								</span>
								<div class="product-item__spec">
									<div class="product-item__spec_line">
										<div class="product-item__spec_item">Вантажопідйомність</div>
										<div class="product-item__spec_item">'.$product['carrying_capacity'].' кг</div>
									</div>
									<div class="product-item__spec_line">
										<div class="product-item__spec_item">Потужність двигуна</div>
										<div class="product-item__spec_item">'.$product['engine_power'].'</div>
									</div>
									<div class="product-item__spec_line">
										<div class="product-item__spec_item">Коробка передач</div>
										<div class="product-item__spec_item">'.$product['transmission_type'].'</div>
									</div>
								</div>
								<span class="product-item__more">Переглянути детальніше</span>
							</a>';
							}
							?>
							
						</div>
					</div>
				</section>
				<?php do_action('contact_form_tamplate_action'); ?>
			</main>
<?php
get_footer();
?>
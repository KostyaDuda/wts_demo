<?php  /* Template Name: About */
get_header();
 ?>
<main class="main">
				<section class="section section-gradient section-gradient--right section-first-padding">
					<div class="container">
						<ul class="breadcrumb">
							<li class="breadcrumb__item">
								<a href="/" class="breadcrumb__link">Головна</a>
							</li>
							<li class="breadcrumb__item">
								<span class="breadcrumb__current">Про компанію</span>
							</li>
						</ul>
						<h1 class="section-title">Про компанію</h1>
						<div class="about">
							<div class="about__text">
								<p>
									Ми, молода та амбітна компанія, яка є представником в Україні провідних європейських брендів комунальної 
									та будівельної техніки. Наша мета об’єднати традиції  машинобудування з сучасними  інженерними   рішеннями 
									та створити власний бренд.
								</p>
								<p>
									Наша компанія, створена під час повномасштабної війни, з метою допомогти містам країни в ліквідації руйнувань, 
									в відновленні та розвитку технологій для забезпечення життєдіяльності. За допомогою техніки, яку ми пропонуємо, 
									прагнемо змінювати обличчя міст. Покращувати житлово-комунальну інфраструктуру і процеси пов’язані з її 
									обслуговуванням, відповідати потребам мешканців громад.
								</p>
								<p>
									Ми пропонуємо широкий асортимент комунальної, спеціальної та будівельної техніки,  інноваційні  рішення для 
									транспортування, будівництва та громадського транспорту.
								</p>
							</div>
							<div class="about__img-box">
								<div class="about__img">
									<img src="<?php echo get_template_directory_uri()?>/img/photo_about.png" alt="photo">
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php do_action('contact_form_tamplate_action'); ?>
</main>
<?php
get_footer();
?>
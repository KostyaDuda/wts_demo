<?php  /* Template Name: Portfolio */
get_header();
 ?>
<main class="main">
				<section class="section section_title-slide">
                    <div class="title-slide">
                        <div class="title-slide_text">
                            <div class="container">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current">Портфоліо</span>
                                    </li>
                                </ul>
                                <h1 class="section-title">Портфоліо</h1>
                            </div>
                        </div>
                        <div class="title-slide_img">
                            <img src="<?php echo get_template_directory_uri()?>/img/title-slide_1.png" alt="">
                        </div>
                    </div>
				</section>
                <section class="section section--pt section-gradient section-gradient--center">
					<div class="container">
                        <div class="video-block_container">
                            <div class="video-block_description">
                                <p>
                                    Ми, молода та амбітна компанія, яка є представником в Україні провідних європейських брендів 
                                    комунальної та будівельної техніки. Наша мета об’єднати традиції  машинобудування з 
                                    сучасними  інженерними   рішеннями та створити власний бренд.
                                </p>
                                <p>
                                    Наша компанія, створена під час повномасштабної війни, з метою допомогти містам країни в 
                                    ліквідації руйнувань, в відновленні та розвитку технологій для забезпечення життєдіяльності. 
                                    За допомогою техніки, яку ми пропонуємо, прагнемо змінювати обличчя міст. Покращувати 
                                    житлово-комунальну інфраструктуру і процеси пов’язані з її обслуговуванням, відповідати 
                                    потребам мешканців громад.
                                </p>
                                <p>
                                    Ми пропонуємо широкий асортимент комунальної, спеціальної та будівельної техніки,  
                                    інноваційні  рішення для транспортування, будівництва та громадського транспорту.
                                </p>
                            </div>
                            <div class="video-block_video">
                                <iframe height="280" src="https://www.youtube.com/embed/nE9OfwpCdrw" title="YouTube video player" style="border:none; overflow:hidden;"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section section--pt">
					<div class="container">
                        <div class="portfolio_container">
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_1.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_2.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_3.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_4.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_5.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_6.png" alt="">
                                <div class="portfolio_item_text">
                                    Автоцистерна
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_7.png" alt="">
                                <div class="portfolio_item_text">
                                    Автогідропіднімач
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_8.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_9.png" alt="">
                                <div class="portfolio_item_text">
                                    Бортова платформа
                                </div>
                            </a>
                            <a href="javascript:void(0);" class="portfolio_item">
                                <img src="<?php echo get_template_directory_uri()?>/img/portfolio-img_10.png" alt="">
                                <div class="portfolio_item_text">
                                    Самоскид
                                </div>
                            </a>

                        </div>
                    </div>
                </section>
                <?php do_action('contact_form_tamplate_action'); ?>
</main>
<?php
get_footer();
?>
<?php  /* Template Name: Product */
get_header();

$data_product = get_product_by_id($_GET['id']);

 ?>
<main class="main">
                <section class="section section-white section_large-top">
                    <div class="container">
                        <div class="product-card">
                            <div class="product-card__img-col">
                                <ul class="breadcrumb breadcrumb-mob">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <a href="/category_product?id=<?php echo $data_product[0]['category_id']?>" class="breadcrumb__link"><?php echo  $data_product[0]['category_name']?></a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current"><?php echo $data_product[0]['name']?></span>
                                    </li>
                                </ul>
                                <div class="prod-card_slider_for">
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery"  href="<?php echo $data_product[0]['img1']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img1']?>" alt="">
                                        </a>
                                    </div>
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery" href="<?php echo $data_product[0]['img2']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img2']?>" alt="">
                                        </a>
                                    </div>
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery" href="<?php echo $data_product[0]['img3']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img3']?>" alt="">
                                        </a>
                                    </div>
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery"  href="<?php echo $data_product[0]['img1']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img1']?>" alt="">
                                        </a>
                                    </div>
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery" href="<?php echo $data_product[0]['img2']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img2']?>" alt="">
                                        </a>
                                    </div>
                                    <div class="prod-card_slide">
                                        <a data-fancybox="gallery" href="<?php echo $data_product[0]['img3']?>"  class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img3']?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="prod-card_slider_nav">
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img1']?>" alt="">
                                        </div>
                                    </div>
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img2']?>" alt="">
                                        </div>
                                    </div>
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img3']?>" alt="">
                                        </div>
                                    </div>
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img1']?>" alt="">
                                        </div>
                                    </div>
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img2']?>" alt="">
                                        </div>
                                    </div>
                                    <div class="prod-card_slide">
                                        <div class="prod-card_slide_inner">
                                            <img src="<?php echo $data_product[0]['img3']?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-card__desc-col">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <a href="/category_product?id=<?php echo $data_product[0]['category_id']?>" class="breadcrumb__link"><?php echo $data_product[0]['category_name']?></a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current"><?php echo $data_product[0]['name']?></span>
                                    </li>
                                </ul>
                                <h1 class="section-title product-name"><?php echo $data_product[0]['name']?></h1>
                                <div class="product-price">
                                    <span>Ціна:</span>
                                    <span class="price">
                                    <?php echo $data_product[0]['price']?>
                                    </span>
                                </div>
                                <div class="prod-tabs">
                                    <ul class="prod-tabs__caption">
                                        <li class="active">Характеристики</li>
                                        <li>Опис моделі</li>
                                        
                                    </ul>
                                    <div class="prod-tabs__content active">
                                        <div class="features_table">
                                            <div class="features_title">
                                                Маса , кг
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Маса транспортного засобу в спорядженому стані, кг
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['weight_of_the_vehicle_in_equipped_condition']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Максимальна маса транспортного засобу, кг
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['maximum_weight_of_the_vehicle']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Маса вантажу, що перевозиться, кг
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['the_weight_of_the_transported_cargo']?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="features_table">
                                            <div class="features_title">
                                                Двигун
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Тип палива
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['fuel_type']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Кількість та розташування циліндрів
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['number_and_arrangement_of_cylinders']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Потужність двигуна
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['engine_power']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Марка, тип
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['brand_type_of_engine']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Ступінь стиснення
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['compression_ratio']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Робочий об'єм циліндрів, см3
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['working_volume_of_cylinders']?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="features_table">
                                            <div class="features_title">
                                                Трансмісія
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Тип
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['transmission_type']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Зчеплення (марка, тип)
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['clutch']?>
                                                </div>
                                            </div>
                                            <div class="features_line">
                                                <div class="features_item">
                                                    Головна передача (тип, маркування)
                                                </div>
                                                <div class="features_item">
                                                <?php echo $data_product[0]['main_gear']?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prod-tabs__content">
                                        <p><?php echo $data_product[0]['description']?></p>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" download="" class="btn btn_download">
                                    Завантажити pdf-файл
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <?php do_action('contact_form_tamplate_action'); ?>
</main>
<?php
get_footer();
?>
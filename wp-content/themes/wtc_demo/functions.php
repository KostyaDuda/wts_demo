<?php
/**
 * wtc_demo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wtc_demo
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wtc_demo_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on wtc_demo, use a find and replace
		* to change 'wtc_demo' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'wtc_demo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'wtc_demo' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'wtc_demo_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'wtc_demo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wtc_demo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wtc_demo_content_width', 640 );
}
add_action( 'after_setup_theme', 'wtc_demo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wtc_demo_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'wtc_demo' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'wtc_demo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'wtc_demo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wtc_demo_scripts() {
	wp_enqueue_style( 'wtc_demo-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'wtc_demo-style', 'rtl', 'replace' );

	wp_enqueue_script( 'wtc_demo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wtc_demo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function save_form_data_to_database() {
    global $wpdb;

    if (isset($_POST['submit'])) {

        $name = sanitize_text_field($_POST['name']);
        $email = sanitize_email($_POST['email']);
        $company = sanitize_text_field($_POST['company']);
        $phone = sanitize_text_field($_POST['phone']);

        $table_name = $wpdb->prefix . 'contact_request'; // Замініть 'form_data' на назву вашої таблиці

        $data = array(
            'name' => $name,
			'company' => $company,
            'email' => $email,
            'phone' => $phone
        );

		

		if($wpdb->insert($table_name, $data)){

			$redirect_url = isset($_POST['redirect_url']) ? esc_url($_POST['redirect_url']) : home_url();
			wp_safe_redirect($redirect_url);
			exit;
		}
    }
}
add_action('template_redirect', 'save_form_data_to_database');

function contact_form_tamplate() {  
    include __DIR__.'/template-parts/contact-form.php';
}

add_action('contact_form_tamplate_action', 'contact_form_tamplate');

function enqueue_custom_pagination_styles() {
    wp_enqueue_style('custom-pagination-style', get_template_directory_uri() . '/css/custom-pagination.css');
}
add_action('wp_enqueue_scripts', 'enqueue_custom_pagination_styles');

function get_list_category_product(){

	global $wpdb;

	$table_name = $wpdb->prefix . 'category_product';

	$query = "SELECT * FROM $table_name";
	$results = $wpdb->get_results($query);

	$data = [];

	foreach ($results as $row) {
		array_push($data,[
		  'id' => $row->Id,
		  'name' =>$row->name
		]);
	}
	return $data;

}

function get_category_by_id($category_id){

	global $wpdb;

	$table_name = $wpdb->prefix . 'category_product';
	$par = 'Id';

	$query = "SELECT * FROM $table_name WHERE $par = $category_id";
	$results = $wpdb->get_results($query);

	$data = [];

	foreach ($results as $row) {
		array_push($data,[
		  'Id' => $row->Id,
		  'name' =>$row->name,
		  'description' => $row->description,
		  'img' => $row->img
		]);
	}
	return $data;

}

function get_list_product_by_categeory($category_id){

	global $wpdb;
	$par = 'category_id';

	$table_name = $wpdb->prefix . 'product';

	$query = "SELECT * FROM $table_name WHERE $par = $category_id";
	$results = $wpdb->get_results($query);

	$data = [];

	foreach ($results as $row) {
		array_push($data,[
		  'id' => $row->Id,
		  'name' =>$row->name,
		  'img1' => $row->img1,
		  'carrying_capacity' => $row->carrying_capacity,
		  'engine_power' => $row->engine_power,
		  'transmission_type' => $row->transmission_type
		]);
	}
	return $data;


}

function get_product_by_id($product_id){
	global $wpdb;
	$par = 'Id';

	$table_name = $wpdb->prefix . 'product'; // Замініть 'your_custom_table' на назву вашої таблиці

	$query = "SELECT * FROM $table_name WHERE $par = $product_id";
	$results = $wpdb->get_results($query);

	$data = [];

	foreach ($results as $row) {
		array_push($data,[
		  'id' => $row->Id,
		  'name' =>$row->name,
		  'img1' => $row->img1,
		  'carrying_capacity' => $row->carrying_capacity,
		  'engine_power' => $row->engine_power,
		  'transmission_type' => $row->transmission_type,
		  'description' => $row->description,
		  'price' => $row->price,
		  'img2' => $row->img2,
		  'img3' => $row->img3,
		  'category_id' => $row->category_id,
		  'category_name' => $row->category_name,
		  'weight_of_the_vehicle_in_equipped_condition' => $row->weight_of_the_vehicle_in_equipped_condition,
		  'maximum_weight_of_the_vehicle' => $row->maximum_weight_of_the_vehicle,
		  'the_weight_of_the_transported_cargo' => $row->the_weight_of_the_transported_cargo,
		  'fuel_type' => $row->fuel_type,
		  'number_and_arrangement_of_cylinders' => $row->number_and_arrangement_of_cylinders,
		  'brand_type_of_engine' => $row->brand_type_of_engine,
		  'compression_ratio' => $row->compression_ratio,
		  'working_volume_of_cylinders' => $row->working_volume_of_cylinders,
		  'clutch' => $row->clutch,
		  'main_gear' => $row->main_gear,
		]);
	}
	return $data;
}










<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wtc_demo
 */
?>
<!doctype html>
<html lang="uk">
<head>
	<meta charset="utf-8">
	<title>Template</title>
	<!-- <meta name="it-rating" it-rating="" content="it-rat-4cc45965d05aec758e327403c1b93530" /> -->
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="theme-color" content="#fff">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>img/favicons/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/css/favicons/favicon.png" type="image/x-icon">
	
	<!--    include fonts    -->
	<!--<link rel="preconnect" href="css/fonts/Roboto.css" crossorigin />-->
	<!--<link rel="preload" as="style" href="css/fonts/Roboto.css" />-->
	<!--<link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" href="css/fonts/Roboto.css" />-->
	<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/css/libs.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fancybox.css">
	<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
	
</head>
<body>
<!-- BEGIN  wrapper-->
<div class="nav-toggle">
	<span></span>
</div>
<div class="header__menu">
	<ul>
		<?php
			$data = get_list_category_product($wpdb);

			foreach ($data as $val){
				echo "<li><a href='/category_product?id=".$val['id']."'>".$val['name']."</a></li>";
			}
		 ?>
	</ul>
	<ul class="mob_header-links">
		<li><a href="/about">Про компанію</a></li>
		<li><a href="/portfolio">Портфоліо</a></li>
		<li><a href="/service">Сервіс та гарантії</a></li>
		<li><a href="/contacts">Контакти</a></li>
	</ul>
</div>
<div id="smooth-wrapper">
	<div id="smooth-content">
		<div class="wrapper <?php 
			if (strpos($_SERVER['REQUEST_URI'], '/product') !== false){
				echo "special_white";
			}?>">
			<header class="header">
				<div class="container">
					<div class="flex-container flex-between flex-middle">
						<a href="/" class="header__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
						</a>
						<div class="header__nav">
							<ul>
								<li><a href="/about">Про компанію</a></li>
								<li><a href="/portfolio">Портфоліо</a></li>
								<li><a href="/service">Сервіс та гарантії</a></li>
								<li><a href="/contacts">Контакти</a></li>
							</ul>
						</div>
						
					</div>
				</div>
				
			</header>
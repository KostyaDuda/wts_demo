"use strict";var body=$("body");$(".nav-toggle").click(function(){var menuItem=$(this);if($(this).hasClass("active")){$(this).parents("body").find(".header__menu").slideUp(300);$(this).parents("body").removeClass("no-scroll");setTimeout(function(){menuItem.addClass("not-active");menuItem.removeClass("active")},100)}else{$(this).parents("body").find(".header__menu").slideDown(300);$(this).parents("body").addClass("no-scroll");setTimeout(function(){menuItem.addClass("active");menuItem.removeClass("not-active")},100)}if($(window).width()>760){gsap.registerPlugin(ScrollTrigger);var boxes=gsap.utils.toArray(".header__menu ul li");boxes.forEach(function(box){gsap.from(box,{scrollTrigger:{trigger:box,scroller:".header__menu",start:"-650 top"},opacity:0,y:150,duration:1,delay:0.3})})}});$(document).ready(function(){$("#input-phone").mask("+38 (099) 999-99-99");/* - - - - - - - - - - - - - -       init slick       - - - - - - - - - - - - - - - */var swiper=new Swiper(".main-slider",{pagination:{el:".swiper-pagination",type:"fraction"},navigation:{nextEl:".main-slider__next",prevEl:".main-slider__prev"},effect:"creative",creativeEffect:{prev:{shadow:true,translate:["-120%",0,-500]},next:{shadow:true,translate:["120%",0,-500]}}});var missionSlider=$(".js-mission-slider");missionSlider.on("init",function(slick){if($(".js-mission-slider .slick-dots").children().length<10){$(".mission-slider__counter .total").text("0"+$(".js-mission-slider .slick-dots").children().length)}else{$(".mission-slider__counter .total").text($(".js-mission-slider .slick-dots").children().length)}});missionSlider.slick({infinite:false,arrows:false,dots:true,slidesToScroll:1,slidesToShow:1});missionSlider.on("afterChange",function(event,slick,currentSlide){var current=currentSlide+1;if(current<10){$(".mission-slider__counter .current").text("0"+current)}else{$(".mission-slider__counter .current").text(current)}});body.on("click",".mission-slider__next",function(){$(".js-mission-slider").slick("slickNext")});body.on("click",".mission-slider__prev",function(){$(".js-mission-slider").slick("slickPrev")});function blogHomeSliderInit(){var blogHome=$(".js-blog-home");if($(window).width()<1199){blogHome.slick({infinite:false,arrows:false,dots:false,slidesToScroll:1,slidesToShow:1,responsive:[{breakpoint:760,settings:{variableWidth:true}}]})}else{blogHome.slick("unslick")}}blogHomeSliderInit();$(".prod-card_slider_for").slick({slidesToShow:1,slidesToScroll:1,arrows:false,fade:true,draggable:false,asNavFor:".prod-card_slider_nav",responsive:[{breakpoint:1020,settings:{centerMode:true}}]});$(".prod-card_slider_nav").slick({slidesToShow:4,arrows:false,slidesToScroll:1,asNavFor:".prod-card_slider_for",variableWidth:true,focusOnSelect:true,responsive:[{breakpoint:760,settings:{slidesToShow:3}}]});gsap.registerPlugin(ScrollTrigger,ScrollSmoother);// create the smooth scroller FIRST!
var smoother=ScrollSmoother.create({smooth:2,// seconds it takes to "catch up" to native scroll position
effects:true// look for data-speed and data-lag attributes on elements and animate accordingly
});// Detect if a link's href goes to the current page
function getSamePageAnchor(link){if(link.protocol!==window.location.protocol||link.host!==window.location.host||link.pathname!==window.location.pathname||link.search!==window.location.search){return false}return link.hash}// Scroll to a given hash, preventing the event given if there is one
function scrollToHash(hash,e){var elem=hash?document.querySelector(hash):false;if(elem){if(e)e.preventDefault();//gsap.to( window, { scrollTo: elem } );
smoother.scrollTo(elem,true)}}// If a link's href is within the current page, scroll to it instead
document.querySelectorAll("a[href]").forEach(function(a){a.addEventListener("click",function(e){scrollToHash(getSamePageAnchor(a),e)})});// Scroll to the element in the URL's hash on load
scrollToHash(window.location.hash)});$(".product-list__more-btn").click(function(){var elem=$(".product-list__more-btn").text();if(elem=="\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u0438 \u0431\u0456\u043B\u044C\u0448\u0435"){$(this).text("\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u0438 \u043C\u0435\u043D\u044C\u0448\u0435");$(this).parents(".product-list").find(".product-item:nth-child(n+7)").slideDown()}else{$(this).text("\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u0438 \u0431\u0456\u043B\u044C\u0448\u0435");$(this).parents(".product-list").find(".product-item:nth-child(n+7)").slideUp()}});$(".popup_close-btn, .popup_bg").click(function(){var popup=$(".done-popup-wrapper");$(this).parents("body").removeClass("no-scroll");setTimeout(function(){popup.removeClass("active")},100)});$(".close_coockie_popup, .popup_close-btn").click(function(){var popup=$(".coockie_popup");setTimeout(function(){popup.addClass("check")},100)});$("ul.prod-tabs__caption").on("click","li:not(.active)",function(){$(this).addClass("active").siblings().removeClass("active").closest("div.prod-tabs").find("div.prod-tabs__content").removeClass("active").eq($(this).index()).addClass("active")});// $("form").submit(function(){
// 	let popup = $(".done-popup-wrapper")
// 	if (popup.hasClass('active')) {
// 		$(this).parents('body').removeClass('no-scroll');
// 		setTimeout(function () {
// 			popup.removeClass('active');
// 		}, 100);
// 	} else {
// 		$(this).parents('body').addClass('no-scroll');
// 		setTimeout(function () {
// 			popup.addClass('active');
// 		}, 100);
// 	}
// return false // предотвращаем отправку формы и перезагрузку страницы
// })
// window.onload = () => {
// 	if (window.matchMedia('(max-width: 1199px)').matches) {
// 	window._mp_smoother.kill();
// 	window.ScrollTrigger.killAll();
// 	} else {}
// }
// gsap.registerPlugin(ScrollTrigger, ScrollSmoother);
// let smoother = ScrollSmoother.create({
// 	wrapper: '#smooth-wrapper',
// 	content: '#smooth-content',
// });
// body.on( 'click', '.nav-toggle', function() {
// 	$( 'body, html' ).toggleClass( 'no-scroll' );
// 	$( this ).removeClass( 'not-active' );
// 	$( this ).toggleClass( 'active' );
// 	$( '.header-nav' ).toggleClass( 'active' );
// })
//# sourceMappingURL=app.js.map

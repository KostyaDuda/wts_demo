<?php  /* Template Name: Contact_Form */

 ?>
				<section class="
				<?php
					if (strpos($_SERVER['REQUEST_URI'], '/contacts') !== false) {
						
						echo "section section-gradient section-gradient--right section-first-padding";
					} elseif(strpos($_SERVER['REQUEST_URI'], '/category_product') !== false) {
						
						echo "section section-gradient section-gradient--left";
					}
					elseif(strpos($_SERVER['REQUEST_URI'], '/product') !== false) {
						
						echo "section section-gradient section-gradient--left";
					}
					else{
						echo "section section--pt section-gradient section-gradient--left";
					}
				?>
				">
					<div class="container">
						<?php 
						if (strpos($_SERVER['REQUEST_URI'], '/contacts') !== false) {
						
							echo '
							<ul class="breadcrumb">
								<li class="breadcrumb__item">
									<a href="/" class="breadcrumb__link">Головна</a>
								</li>
								<li class="breadcrumb__item">
									<span class="breadcrumb__current">Контакти</span>
								</li>
							</ul>';
						}
						?>

						<div class="section-title">Звʼязок із нами</div>
						<div class="contacts-home-container">
							<div class="contacts-home">
								<div class="contacts-home-item">
									<div class="contacts-home-item__label">Адреса офісу</div>
									<div class="contacts-home-item__val">Київ, вул Джона Маккейна 40, офіс 1</div>
								</div>
								<div class="contacts-home-item">
									<div class="contacts-home-item__label">Адреса виробництва</div>
									<div class="contacts-home-item__val">Кременчук, вул. Київська 64а</div>
								</div>
								<div class="contacts-home-item">
									<div class="contacts-home-item__label">Телефон відділу продажу</div>
									<a href="tel:+380984445656;javascript:void(0);" class="contacts-home-item__val">+38 098 444 56 56</a>
									<a href="tel:+380934445656;javascript:void(0);" class="contacts-home-item__val">+38 093 444 56 56</a>
								</div>
								<div class="contacts-home-item">
									<div class="contacts-home-item__label">Телефон відділу співробітництва</div>
									<a href="tel:+380984445656;javascript:void(0);" class="contacts-home-item__val">+38 098 444 56 56</a>
									<a href="tel:+380934445656;javascript:void(0);" class="contacts-home-item__val">+38 093 444 56 56</a>
								</div>
							</div>
							<div class="contacts-home__form">
								<div class="contact-form">
									<div class="contact-form__title">Заповніть форму</div>
									<form  method="post">
										<div class="input-wrap input-wrap--half">
											<label>Ваше імʼя*</label>
											<input type="text" name="name" placeholder="Введіть імʼя" autocomplete="off" required>
										</div>
										<div class="input-wrap input-wrap--half">
											<label>Компанія</label>
											<input type="text" name="company" placeholder="Введіть назву" autocomplete="off">
										</div>
										<div class="input-wrap">
											<label>Ваш телефон*</label>
											<input type="tel" name="phone" id="input-phone" placeholder="Введіть телефон" autocomplete="off" required>
										</div>
										<div class="input-wrap">
											<label>Адреса електронної пошти</label>
											<input type="email" name="email" placeholder="Введіть e-mail" autocomplete="off">
										</div>
										<input type="hidden" name="redirect_url" value="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
										<button type="submit" name="submit" class="btn btn_submit">Надіслати запит</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
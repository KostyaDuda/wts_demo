
                
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <main class="main">
    <section class="section section_title-slide">
                    <div class="title-slide">
                        <div class="title-slide_text">
                            <div class="container">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb__item">
                                        <a href="/" class="breadcrumb__link">Головна</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <a href="/blog" class="breadcrumb__link">Блог</a>
                                    </li>
                                    <li class="breadcrumb__item">
                                        <span class="breadcrumb__current">Стаття</span>
                                    </li>
                                </ul>
                                <h1 class="section-title">Стаття</h1>
                            </div>
                        </div>
                        <div class="title-slide_img">
                            <img src="<?php echo get_template_directory_uri()?>/img/title-slide_3.png" alt="">
                        </div>
                    </div>
				</section>
                <section class="section section--pt">
					<div class="container">
                        <div class="article">
                            <div class="article_date">
                            <?php wtc_demo_posted_on();?>
                            </div>
                            <h2 class="article_title">
                            <?php the_title();?>
                            </h2>

                            <?php the_content(); ?>


                        </div>
                    </div>
                </section>

                <?php do_action('contact_form_tamplate_action');?>

    </main>
    </article><!-- #post-<?php the_ID(); ?> -->
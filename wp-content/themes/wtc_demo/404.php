<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wtc_demo
 */

get_header();
?>

<main class="main">
				<section class="section section-gradient section-gradient--center-right section-first-padding section-404">
                    <div class="bg-404">
                        <img src="<?php echo get_template_directory_uri()?>/img/404-bg.svg" alt="background image">
                    </div>
					<div class="container">
                        <div class="wrap-404">
                            <h1 class="section-title">404</h1>
                            <p>Сторінку, яку ви шукали не знайдено!</p>
                            <a href="/" class="btn">Повернутись на головну</a>
                        </div>
                    </div>
				</section>
			</main>

<?php
get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wtc_demo
 */

?>

				<footer class="footer">
				<div class="container">
					<div class="flex-container flex-between flex-top">
						<div class="footer__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
						</div>
						<div class="footer__nav">

								<?php
									$data = get_list_category_product($wpdb);
									$list_category_html='<div class="footer__column"><ul>';

									foreach ($data as $key => $val){
										if($key == 5){
											$list_category_html .= '</ul></div><div class="footer__column"><ul>';
										}
										else{
											$list_category_html .= "<li><a href='/category_product?id=".$val['id']."'>".$val['name']."</a></li>";
										}
										
									}
									$list_category_html .= '</ul></div>';
									echo $list_category_html;
								?>
							<div class="footer__column">
								<ul>
									<li><a href="/about">Про компанію</a></li>
									<li><a href="/portfolio">Портфоліо</a></li>
									<li><a href="/service">Сервіс та гарантії</a></li>
									<li><a href="/contacts">Контакти</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			
				<div class="footer__bottom">
					<div class="container">
						<div class="flex-container flex-between flex-middle">
							<div class="copyright">© <?php echo date('Y')?> WinTechSolution. Усі права захищені</div>
							<div class="footer__wss">
								<a href="https://web-systems.solutions/" target="_blank" >
									<img src="<?php echo get_template_directory_uri(); ?>/img/wss.svg" alt="wss">
								</a>
								<div class="footer__wss-label">Якісні рішення на основі ефективних технологій</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
			
		</div>
	</div>
</div>

<!-- END wrapper-->
<div class="done-popup-wrapper">
	<div class="popup_bg"></div>
	<div class="popup_item">
		<h3 class="popup_title">
			Дякуємо! Ваш запит 
			успішно відправлено!
		</h3>
		<p>
			Наш менеджер звʼяжеться з вами найближчим часом для уточнення деталей
		</p>
		<div class="popup_bg-img">
			<img src="<?php echo get_template_directory_uri(); ?>/img/popup_bg-img.svg" alt="">
		</div>
		<a href="javascript:void(0);" class="popup_close-btn">
			<i class="icon icon-i-close"></i>
		</a>
	</div>
</div>
<!-- <div class="coockie_popup">
	<p>
		Ми використовуємо файли cookie, щоб забезпечити найкращий досвід користувача 
		на нашому сайті. Натисніть «Прийняти», щоб прийняти всі файли cookie. Ви можете 
		детальніше ознайомитись з політикою використання Cookie.
	</p>
	<a href="javascript:void(0);" class="btn close_coockie_popup">Прийняти</a>
	<a href="javascript:void(0);" class="popup_close-btn">
		<i class="icon icon-i-close"></i>
	</a>
</div> -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/inputmask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.4/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.4/ScrollTrigger.min.js"></script>
<script src="https://unpkg.com/gsap@3/dist/ScrollToPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.4/CustomEase.min.js"></script>
<script src="https://webflow-assets.sfo2.cdn.digitaloceanspaces.com/resonant-link/ScrollSmoother.min.js"></script>
<script src="https://webflow-assets.sfo2.cdn.digitaloceanspaces.com/resonant-link/split-type.js"></script>
<script src="https://webflow-assets.sfo2.cdn.digitaloceanspaces.com/resonant-link/script.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>


</body>
</html>

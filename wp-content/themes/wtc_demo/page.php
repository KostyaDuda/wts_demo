<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wtc_demo
 */

get_header();
?>
			<main class="main">
				<div class="main-slider">
					<div class="swiper-wrapper">

					<?php 
					if(have_rows('slider')):
					while(have_rows('slider')): the_row();?>

						<div class="main-slide swiper-slide">
							<img src="<?php the_sub_field('img'); ?>" alt="banner">
							<h1 class="main-slide__title"><?php the_sub_field('tittle_slider'); ?></h1>
							<div class="main-slider__caption"><?php the_sub_field('caption_slider'); ?></div>
						</div>
					<?php endwhile; endif; ?>
					</div>
					<div class="main-slider__bottom">
						<div class="main-slider__info">
							
							<!-- <div class="main-slider__counter">
								<span class="current">01</span><span class="slash">/</span><span class="total"></span>
							</div> -->
							<div class="swiper-pagination"></div>
						</div>
						<div class="main-slider__nav">
							<div class="main-slider__prev">
								<i class="icon icon-arrow-left"></i>
							</div>
							<div class="main-slider__next">
								<i class="icon icon-arrow-right"></i>
							</div>
						</div>
					</div>
				</div>
				<section class="section">
					<div class="container">
						<h3 class="section-title">Наша продукція</h3>
						<div class="products-main">
						<?php 
								if(have_rows('slider_productions')):
								while(have_rows('slider_productions')): the_row();?>

								<a href="javascript:void(0);" class="product-item">
									<span class="product-item__title"><?php the_sub_field('product_tittle'); ?></span>
									<span class="product-item__pct">
										<img src="<?php the_sub_field('img_background_product'); ?>" alt="<?php the_sub_field('product_tittle'); ?>">
										<span class="product-item__pct second_pct">
											<img src="<?php the_sub_field('image_product'); ?>" alt="<?php the_sub_field('product_tittle'); ?>">
										</span>
									</span>
									
									<span class="product-item__more">Детальніше</span>
								</a>
						<?php endwhile; endif; ?>
							
						</div>
					</div>
				</section>
				<section class="section section--pt section-gradient section-gradient--center">
					<div class="container">
						<h3 class="section-title">Компанія сьогодні</h3>
						<div class="achievements">
							<?php 
								if(have_rows('achievements')):
								while(have_rows('achievements')): the_row();?>
								<div class="achievement-item">
									<div class="achievement-item__val"><?php the_sub_field('achievement_val'); ?></div>
									<div class="achievement-item__desc"><?php the_sub_field('tittle_achievement'); ?></div>
								</div>
						<?php endwhile; endif; ?>
						</div>
					</div>
				</section>
				<section class="section section--pt">
					<div class="container">
						<h3 class="section-title">Наша місія та цінності</h3>
						<div class="mission-container">
							<div class="mission-wrap">
								<div class="mission-slider__counter">
									<span class="current">01</span><span class="slash">/</span><span class="total"></span>
								</div>
								<div class="mission-slider js-mission-slider">
							<?php 
							if(have_rows('mission-slider')):
							while(have_rows('mission-slider')): the_row();?>
									<div class="mission-slide">
											<div class="mission-slide__title"><?php the_sub_field('tittle_mission_lider'); ?></div>
											<div class="mission-slide__desc">
												<p>
												<?php the_sub_field('caption_mission_lider'); ?>
												</p>
											</div>
										</div>
							<?php endwhile; endif; ?>
								</div>
								<div class="mission-slider__nav">
									<div class="mission-slider__prev">
										<i class="icon icon-arrow-left"></i>
									</div>
									<div class="mission-slider__next">
										<i class="icon icon-arrow-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section section--pt">
					<div class="container">
						<div class="section-title">Бренди, які ми представляємо</div>
						<div class="partners-container">
							<div class="partners">
								<?php 
								if(have_rows('partners')):
								while(have_rows('partners')): the_row();?>
								<a href="javascript:void(0);" class="partner-item">
									<span class="partner-item__logo">
										<img src="<?php the_sub_field('partners_logo'); ?>" alt="partner-1">
									</span>
									<span class="partner-item__desc"><?php the_sub_field('partners_caption'); ?></span>
									<span class="partner-item__more">Детальніше</span>
								</a>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</section>
				<section class="section section--pt">
					<div class="container">
						<div class="section-top section-top_blog-home">
							<div class="section-title">Наш блог</div>
							<a href="/blog" class="blog_link">Переглянути більше</a>
						</div>
						<div class="blog-home js-blog-home">
						<?php
							global $wpdb;
							$query = "SELECT * FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 3";
							$posts = $wpdb->get_results($query);

							if ($posts) {
								foreach ($posts as $post) {
									
									$image_url = get_the_post_thumbnail_url($post->ID, 'full');
									echo '
									<a href="' . get_permalink() . '" class="blog-home-item">
									<span class="blog-home-item__pct">
										<img src="' . esc_url($image_url) . '" alt="blog-home-1">
									</span>
									<span class="blog-home-item__title">'.get_the_title().'</span>
									<span class="blog-home-item__desc">'.get_the_excerpt().'</span>
									<span class="blog-home-item__more">Детальніше</span>
								</a>';
								}
							} else {
								echo "Постів не знайдено.";
							}
						?>
						</div>
						<a href="/blog" class="blog_link blog_link-mob">Переглянути більше</a>
					</div>
				</section>
				<?php do_action('contact_form_tamplate_action'); ?>
			</main>

<?php
get_footer();
?>
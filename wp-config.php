<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wtc_demo' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&+Fj1aI-e/-9%d%fO>xOE%s2@VX/AeiTC&pV,/VI6*/eQLv?w_Ah]1<WmWo%,[pq' );
define( 'SECURE_AUTH_KEY',  '}WKTQJZR`Ho6IL{hj(YoceY(IFWtOOf7~Gp1-6oE}AotcapV{Z[+28^8a/b_kkp)' );
define( 'LOGGED_IN_KEY',    'arNu`>Hh,9q^,D6!G^ENP)=O#VXHI5ty{G^Y-FI|$eax4SJ!gll&#gv6]%82!RN?' );
define( 'NONCE_KEY',        'uS2jekuAnw[O5z,?}}(6LQ]2!~?]dWl2E0/guNB%DDv[F/)dVSbp9oCPlKK7k:*?' );
define( 'AUTH_SALT',        'Xa&~L).#<^Paiw#9d|Zc6+5oomSCEc} H6ze+fgFaNVhfs=$;cW=RucpW`)HcC5m' );
define( 'SECURE_AUTH_SALT', 'kH869[}#e{O(f^OzctO2/K2G<Y[~:D&VEx{6HT 9h#)]AxDAbad [zd``12A ZHt' );
define( 'LOGGED_IN_SALT',   '[IPJy=r2()>foBh81FM>xDoqr`~A{3mbbd_Z#Yp@7RR`9PGx^?8?M%&nXIav$*yR' );
define( 'NONCE_SALT',       '/ytQeAm*(}PUpDUQkAyvj]UfPJ`]NCieifW:sPE<K0 :72JxB=GwP;3APb(8Z:YF' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
